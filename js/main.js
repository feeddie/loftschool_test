$(document).ready(function() {

	(function() {
		var button = $('.contacts__button');

		button.on("click", function (e) {
			e.preventDefault();
			var block = $('.contacts__content');
			if(block.is(":hidden")) {
				block.removeClass('animated bounceOutRight');
				block.show();
				block.addClass('animated bounceInRight');
			} else if(block.is(":visible")) {
				block.addClass('animated bounceOutRight');
				setTimeout(function () {
					block.hide();
				}, 700)
			}		
		});
	})();

	(function() {
		$('.career__item').on('click', function() {
			var 
				that = $(this),
				img = $('.career__pic-img'),
				path;

			that
				.addClass('career__item_active')
				.siblings()
				.removeClass('career__item_active');

			if(that.hasClass('koek')){
				path = "img/contents/kgk.png"
			} else if (that.hasClass('nvision')) {
				path = "img/contents/n.jpg"
			} else if (that.hasClass('genser')) {
				path = "img/contents/g.png"
			}

			img
				.fadeOut(function() {
					img
						.attr('src', path)
						.fadeIn();
				})
				
		});
	})();

});